import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
public class BasicOperation {

    private static final Logger logger = LogManager.getLogger(BasicOperation.class);
    private static Scanner scanner = new Scanner(System.in);
    private HikariDataSource dataSource;

    private BasicOperation()  {
        HikariConfig config = new HikariConfig();

        config.setJdbcUrl( "jdbc:postgresql://localhost/internship_samples" );
        config.setUsername( "postgres" );
        config.setPassword( "7890" );
        config.addDataSourceProperty( "cachePrepStmts" , "true" );
        config.addDataSourceProperty( "prepStmtCacheSize" , "250" );
        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
        dataSource = new HikariDataSource( config );

    }

    public static void main(String[] args) {

        BasicOperation basic = new BasicOperation();
        char ch;

        do {
            LocalDateTime currentTime = LocalDateTime.now();
            logger.info("The current time is {}", currentTime);

            logger.info("Instructions:-");

            logger.info("Insert : For inserting 10 rows in table.");
            logger.info("SelectFirstRow :  For selecting first row from table.");
            logger.info("SelectAll : For selecting all row from table.");
            logger.info("UpdateFirstRow : For Updating first rows from table.");
            logger.info("UpdateAll : For updating all row from table.");
            logger.info("DeleteFirstRow : For deleting first rows from table.");
            logger.info("DeleteAll : For deleting all row from table.");

            logger.info("Please Enter the Choice:-");
          String choice = scanner.next();
            switch (choice) {

                case "Insert":
                    basic.insertData();

                    LocalDateTime afterTime = LocalDateTime.now();
                    logger.info("After execution time is {}", afterTime);
                    Duration duration = Duration.between(currentTime, afterTime);
                    logger.info("Total execution time is : {}", duration);
                    break;
                case "SelectFirstRow":
                    FunctionInterface function = basic::selectFirstData;
                    function.selectAllData();

                    LocalDateTime afterSelectFirstRow = LocalDateTime.now();
                    logger.info("After execution time is : {}", afterSelectFirstRow);
                    Duration firstDuration = Duration.between(currentTime, afterSelectFirstRow);
                    logger.info("Total execution time is : {}", firstDuration);
                    break;
                case "SelectAll":
                    basic.lambdaExpression();

                    LocalDateTime selectAllTime = LocalDateTime.now();
                    logger.info("After execution time is {}", selectAllTime);
                    Duration selectAllDuration = Duration.between(currentTime, selectAllTime);
                    logger.info("Total execution time is : {}", selectAllDuration);
                    break;
                case "UpdateFirstRow":
                     basic.updateFirstData();

                    LocalDateTime updateFirstTime = LocalDateTime.now();
                    logger.info("After execution time is {}", updateFirstTime);
                    Duration updateFirstDuration = Duration.between(currentTime, updateFirstTime);
                    logger.info("Total execution time is : {}", updateFirstDuration);

                    break;
                case "UpdateAll":
                    basic.updateAllData();

                    LocalDateTime updateAllTime = LocalDateTime.now();
                    logger.info("After execution time is {}", updateAllTime);
                    Duration updateAllDuration = Duration.between(currentTime, updateAllTime);
                    logger.info("Total execution time is : {}", updateAllDuration);

                    break;
                case "DeleteFirstRow":
                    basic.deleteFirstData();

                    LocalDateTime deleteFirstTime = LocalDateTime.now();
                    logger.info("After execution time is {}", deleteFirstTime);
                    Duration deleteFirstDuration = Duration.between(currentTime, deleteFirstTime);
                    logger.info("Total execution time is : {}", deleteFirstDuration);

                    break;
                case "DeleteAll":
                    basic.deleteAllData();

                    LocalDateTime deleteAllTime = LocalDateTime.now();
                    logger.info("After execution time is {}", deleteAllTime);
                    Duration deleteAllDuration = Duration.between(currentTime, deleteAllTime);
                    logger.info("Total execution time is : {}", deleteAllDuration);

                    break;
                default:
                    logger.warn("Please Enter Valid choice");
                    break;
            }

            logger.info("Do you want to continue (y/n)");
            ch = scanner.next().charAt(0);
        } while (ch == 'Y' || ch == 'y');
    }

    private void lambdaExpression(){

        FunctionInterface functionInterface;
        functionInterface = () -> {

            final String query = "select * from users where id between 101 and 110 order by id";
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement statement = connection.prepareStatement(query);
                 ResultSet result = statement.executeQuery()) {

                while (result.next()) {

                    int id = result.getInt("id");

                    String name = result.getString("name");

                    int salary = result.getInt("salary");

                    logger.info("User Id {}, Name {} and Salary {}", id, name, salary);
                }

            } catch (SQLException e) {
                logger.error(e.getMessage());
            }finally {
                if (dataSource != null) {
                    dataSource.close();
                }
            }
        };
        functionInterface.selectAllData();
    }

    private void insertData() {

       final String query = "insert into users values(?,?,?)";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {

            for (int i = 101; i <= 110; i++) {
                logger.info("Enter the id");
                int id = scanner.nextInt();

                logger.info("Enter the name");
                String name = scanner.next();

                logger.info("Enter the salary");
                int salary = scanner.nextInt();

                statement.setInt(1, id);
                statement.setString(2, name);
                statement.setInt(3, salary);

              int  result = statement.executeUpdate();

                if (result == 0) {
                    logger.error("User details are not inserted");
                } else {
                    logger.info("User details are inserted");
                }
                logger.info("{} Rows Inserted", result);
            }



        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        finally {
            if (dataSource != null) {
                dataSource.close();
            }
        }
    }


    private void updateAllData() {

       final String query = "update users set salary = ? where id between ? and ?";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setInt(1, 500_00);
            statement.setInt(2, 101);
            statement.setInt(3, 110);

            int result = statement.executeUpdate();

            logger.info("{} Rows Updated.", result);

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        finally {
            if (dataSource != null) {
                dataSource.close();
            }
        }
    }

    private void deleteFirstData() {

       final String query = "delete from users where id = ?";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setInt(1, 101);
            int delete = statement.executeUpdate();

            logger.info("{} Rows deleted.", delete);

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        finally {
            if (dataSource != null) {
                dataSource.close();
            }
        }
    }

    private void deleteAllData() {

        final String query = "delete from users";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {

            final int delete = statement.executeUpdate();

            logger.info("{} Rows deleted.", delete);

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        finally {
            if (dataSource != null) {
                dataSource.close();
            }
        }
    }

    private void selectFirstData() {

        final String query = "select * from users where id between 101 and 108 order by id";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet result = statement.executeQuery()) {

            List<User> users = new ArrayList<>();

            while (result.next()) {

                int id = result.getInt("id");
                int salary = result.getInt("salary");
                String name = result.getString("name");

                User user = new User();

                user.setId(id);
                user.setName(name);
                user.setSalary(salary);

                users.add(user);

            }

            users.forEach(userList -> logger.info("{}", users));

            List<User> sort = users.stream().sorted(Comparator.comparing(User::getName).reversed()).collect(Collectors.toList());
             users.sort(new UserComparator());

             logger.info("Sorted Name by using stream as well as Collection");
             logger.info("Sorted Name is :{}",sort);
            logger.info("Sorted Name is : {}", users);

            List<User> userList = users.stream().filter(userId -> userId.getId() == 101).collect(Collectors.toList());

            for (User user : userList) {

                logger.info("User Id is : {}, name: {} and salary: {}", user.getId(), user.getName(), user.getSalary());
            }

            String userName =users.get(1).getName();
            Optional<String> checkNull = Optional.ofNullable(userName);

            if (!checkNull.isPresent()) {

                return;
            }
            userName = users.get(2).getName();
            Base64.Encoder encoder = Base64.getEncoder();
          String name = encoder.encodeToString(userName.getBytes(StandardCharsets.UTF_8));

            logger.info("Encoded String is : {}", name);
            Base64.Decoder decoder = Base64.getDecoder();


            String decodString = new String(decoder.decode(name));
            logger.info("Decoded String is : {}", decodString);

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        finally {
            if (dataSource != null) {
                dataSource.close();
            }
        }
    }

    private void updateFirstData() {

      final String query = "update users set salary= ? where id = ?";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setInt(1, 400_00);
            statement.setInt(2, 101);
           int result = statement.executeUpdate();

            logger.info("{} Rows Updated.", result);

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        finally {
            if (dataSource != null) {
                dataSource.close();
            }
        }
    }

}