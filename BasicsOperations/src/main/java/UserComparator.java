import java.util.Comparator;
public class UserComparator implements Comparator<User> {

    public int compare(User nameOne, User nameTwo){
        return nameTwo.getName().compareTo(nameOne.getName());
    }
}
