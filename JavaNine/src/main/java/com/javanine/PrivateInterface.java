package com.javanine;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

public class PrivateInterface implements Numbers{

    @Override
    public void multiplication(int numberOne, int numberTwo) {

        logger.info("Multiplication of two number is : {}",(numberOne * numberTwo));

    }

private static void tryWithResource() throws IOException {

    String fileName = "JavaNine/src/main/resources/content.txt";

    Stream<String> stream = Files.lines(Paths.get(fileName));
    try (stream){

        stream.forEach(logger::info);
    }catch (Exception e){
        logger.error(e.getMessage());
    }
}

private static void diamondOperator(){

       AbstractClass<String> abstractClass = new AbstractClass<>() {

           String show() {
               return "Java" + "9";
           }
       };

   String version =  abstractClass.show();
   logger.info("{}",version);
}

  private static void underscore(){
        int number = 10;//In Java 9, underscore as variable name won’t work altogether.
        logger.info("{}",number);
}

@SafeVarargs
private static void safeVar(List<String>...team){

        logger.info("{}", team);
}

private static void factoryMethod(){

    Set<Integer> numbers = Set.of(2,4,3,5,6);

    for (int number : numbers){
        logger.info("Number is {}",number);
    }
}

private static void processApiUpdate(){
        Long processId = ProcessHandle.current().pid();
        logger.info("Process id is :{}",processId);

}

    public static void main(String[] args) throws IOException {

        Numbers numbers = new PrivateInterface();
        numbers.multiplication(4, 6);
        numbers.addition(2, 4);
        PrivateInterface.tryWithResource();
        PrivateInterface.diamondOperator();
        PrivateInterface.underscore();
        PrivateInterface.factoryMethod();
        PrivateInterface.processApiUpdate();

        List<String> list = new ArrayList<>(Arrays.asList("IND","END","AUS","NZ","PAK","SA","WI","BAN"));
        PrivateInterface.safeVar(list);


    }

}
