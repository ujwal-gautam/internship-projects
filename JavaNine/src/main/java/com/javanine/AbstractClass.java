package com.javanine;

abstract class AbstractClass<T> {

    abstract T show();

}
