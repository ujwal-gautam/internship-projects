package com.javanine;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@FunctionalInterface
public interface Numbers {

    Logger logger = LogManager.getLogger(Numbers.class);

     void multiplication(int numberOne, int numberTwo);

    default void addition( int numberOne, int numberTwo){

        logger.info("Addition of two number is : {}",(numberOne + numberTwo));
        subtraction();
        division();
    }

    private void subtraction()
    {
        logger.info("Substitution of two number is : {}", (10 - 8));

    }

    private void division(){
        logger.info("division of two number is : {}", (10 / 2));

    }

}
