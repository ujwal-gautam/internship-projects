import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.time.LocalDateTime;

public class Advance {
    private static final Logger logger = LogManager.getLogger(Advance.class);

    public static void main(String[] args) {

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl( "jdbc:postgresql://localhost/internship_samples" );
        config.setUsername( "postgres" );
        config.setPassword( "7890" );
        config.addDataSourceProperty( "cachePrepStmts" , "true" );
        config.addDataSourceProperty( "prepStmtCacheSize" , "250" );
        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );

        HikariDataSource dataSource = new HikariDataSource( config );

        LocalDateTime currentTime = LocalDateTime.now();
        logger.info("the current time is {}",currentTime);
        advanceOperation operation = () ->{
            String query = "update users set salary= ? where id between ? and ?";
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {

                statement.setInt(1,500_00);
                statement.setInt(2,101);
                statement.setInt(3,110);

                int updated = statement.executeUpdate();

                try (ResultSet result = statement.getGeneratedKeys()) {
                    logger.info("{} records updated.", updated);

                    while (result.next()) {
                        int id = result.getInt("id");

                        String name = result.getString("name");

                        int salary = result.getInt("salary");

                        logger.info("User Id {}, Name {} and Salary {}", id, name, salary);
                    }
                }
            }
            catch (SQLException e) {
                logger.error(e.getMessage());
            }
            finally {
                dataSource.close();
            }
        };
        operation.show();

        LocalDateTime afterTime = LocalDateTime.now();
        logger.info("After updating time is {}",afterTime);
    }
}