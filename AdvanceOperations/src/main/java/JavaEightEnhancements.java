import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;


public class JavaEightEnhancements{
    private static final Logger logger = LogManager.getLogger(JavaEightEnhancements.class);

    public static void main(String[] args) {

        JavaEightEnhancements javaEight = new JavaEightEnhancements();
         Scanner scanner = new Scanner(System.in);
         char ch;
        do {
            String choice;
            logger.info("Instructions:-");

            logger.info("LambdaExpression: Example of Lambda Expression.");
            logger.info("MethodReferences: Example of Method References.");
            logger.info("JavaStream: Example of Stream.");
            logger.info("OptionalClass: Example of Optional Class.");
            logger.info("Base64: Example of base64.");
            logger.info("DataAndTime: Example of Local Data And TIme in java-8.");
            logger.info("ParallelArray : Example of Parallel Array");
            logger.info("ForEach : Example of forEach loop");

            choice = scanner.next();
            switch (choice){
                case "LambdaExpression":
                    JavaEightEnhancements.lambdaExpression();
                    break;
                case "MethodReferences":
                  JavaEightEnhancements enhancements = new JavaEightEnhancements();
                  JavaEight eight = enhancements::methodReferences;
                  eight.show();
                    break;
                case "JavaStream":
                    javaEight.javaEightStream();
                    break;
                case "OptionalClass":
                    javaEight.optionalClass();
                    break;
                case "Base64":
                    javaEight.base64();
                    break;
                case "DataAndTime":
                    javaEight.localDateTime();
                    break;
                case "ParallelArray":
                    javaEight.parallelArray();
                case "ForEach":
                    javaEight.forEachExample();
                    break;
                default:
                    logger.warn("Enter the Right choice");
                    break;
            }

            logger.info("Do you wont to Continue (y/n) ?");
                ch = scanner.next().charAt(0);
        }while ((ch == 'y' || ch =='Y'));

    }

    private static void lambdaExpression(){
        JavaEight java = ()-> {
            Scanner scanner = new Scanner(System.in);
            logger.info("Enter the any Tow Number");
            int a = scanner.nextInt();
            int b = scanner.nextInt();

           int c = a + b;
           logger.info("Addition of two number : {}",c);
        };
        java.show();

    }

    private void methodReferences(){

            logger.info("Example of Method References");
        }


      private void javaEightStream(){

          List<Integer> number = new ArrayList<>();
          for (int i= 1; i<=10; i++) {
              number.add(i);
          }
          logger.info("the number is {}",number);

              List<Integer> filter = number.stream().filter(i->i%2 == 0).collect(Collectors.toList());
              List<Integer> mapping = number.stream().map(i->i * 3).collect(Collectors.toList());
              List<Integer> sort = number.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());

              //TODO sort desc using stream
              //TODO covert list to set using stream

              logger.info("Even numbers is {}",filter);
              logger.info("{}",mapping);
              logger.info("{}",sort);

              number.add(1);
              number.add(2);
              number.add(3);
              number.add(3);

              logger.info("{}",number);

          Set<Integer> show = number.stream().filter(n ->n%2 == 0).collect(Collectors.toSet());
          logger.info("sorted number is {}",show);

//              logger.info("the number list is {}", show);
          }

          private void optionalClass(){

        //TODO use string instead of arrays
              String[] name = new String[10];
              Optional<String> checkNull = Optional.ofNullable(name[5]);
              if(checkNull.isPresent()){
                  String lowercaseString = name[5].toLowerCase();
                 logger.info("{}",lowercaseString);
              }else
                  logger.info("string value is not present");
          }


          private void base64(){

             //TODO variable names
              //TODO how to encode/decode using Base64 in java7

              Base64.Encoder encoder = Base64.getEncoder();
              String encode = encoder.encodeToString("Java Eight Enhancement".getBytes());
              logger.info("Encoded String: {}",encode);

              Base64.Decoder decoder = Base64.getDecoder();

              String decode = new String(decoder.decode(encode));
              logger.info("Decoder String is {}",decode);
          }


    private void localDateTime(){

        //TODO explore some more
        LocalDateTime currentTime = LocalDateTime.now();
        logger.info("the current time is {}",currentTime);

        LocalDate date = currentTime.toLocalDate();
        logger.info("the current date is {}",date);

        Month month = currentTime.getMonth();
        int day = currentTime.getDayOfMonth();
        int hour = currentTime.getHour();
        int seconds = currentTime.getSecond();

        logger.info("the current month is {}, date is {},hour is {} and seconds is {}",month, day, hour, seconds);

        ZonedDateTime dateOne = ZonedDateTime.parse("2007-12-03T10:15:30+05:30[Asia/Karachi]");
        logger.info("date is : {}" ,dateOne);

        ZoneId id = ZoneId.of("Europe/Paris");
      logger.info("{}",id);

        ZoneId currentZone = ZoneId.systemDefault();
        logger.info("currentTime is {}",currentZone);


        LocalDate today = LocalDate.now();
        logger.info("Current date: {}" ,today);

        LocalDate nextWeek = today.plus(1, ChronoUnit.WEEKS);
       logger.info("Next week: {}" ,nextWeek);

        LocalDate nextMonth = today.plus(1, ChronoUnit.MONTHS);
        logger.info("Next month: {}",nextMonth);

        LocalDate nextYear = today.plus(1, ChronoUnit.YEARS);
        logger.info("Next year: {}" ,nextYear);

        LocalDate nextDecade = today.plus(1, ChronoUnit.DECADES);
        logger.info("Date after ten year: {}",nextDecade);
    }
    private void parallelArray(){

        //TODO For each example

     String[] names = {"akshay","Amol","Sudarshan","Rakesh","Akhileash"};

        for (String name: names) {
            logger.info("The name of Student is {}",name);
        }
        Arrays.parallelSort(names,0,3);

        logger.info("the Sorting name of Students is {}",names);

        for (String sortedname : names) {

            logger.info("Sorted Student is : {}",sortedname);
        }

    }
    private void forEachExample(){

        List<String> list = new ArrayList<>();

        list.add("Akshay");
        list.add("Sudarshan");
        list.add("Akhilesh");
        list.add("Amol");

        list.forEach(name -> logger.info("{}",name));



    }

}
