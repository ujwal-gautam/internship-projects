import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;

public class JavaSevenEnhancements {

    private static final Logger logger = LogManager.getLogger(JavaSevenEnhancements.class);

    private static final String URL = "jdbc:postgresql://localhost/internship_samples";
    private static final String USER = "postgres";
    private static final String PASSWORD = "7890";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        JavaSevenEnhancements java = new JavaSevenEnhancements();
        char ch;
        do {
            logger.info("1. Example of Binary Literals.");
            logger.info("2. Example of Underscores in Numeric Literals.");
            logger.info("3. Example of Strings in switch Statements.");
            logger.info("4. Example of Generic Instance Creation.");
            logger.info("5. Example of The try-with-resources Statement.");
            logger.info("6. Example of Catching Multiple Exception.");
            logger.info("7. Example of SafeVarargs.");

            logger.info("Enter the number:-");
            int choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    java.binaryLiteral();
                    break;
                case 2:
                    java.literalNumeric();
                    break;
                case 3:
                    java.stringSwitch();
                    break;
                case 4:
                    JavaSevenEnhancements.genericInstance();
                    break;
                case 5:
                    java.tryWithResource();
                    break;
                case 6:
                    java.multipleCatch();
                    break;
                case 7:

                    varargs("India","United State");
                    varargs("IND");

                    break;
                default:
                    logger.info("Please Enter the valid Number.");
                    break;
            }
            logger.info("Do you want to continue (y/n) ?");
            ch = scanner.next().charAt(0);
        } while (ch == 'Y' || ch == 'y');
    }

    private void binaryLiteral() {

        int a = 0b0111;
        byte b = (byte) 0b0111;// b can be lower or upper case
        long l = (long) 0b0111;
        byte by = 0b101_0;

        logger.info("a = {}", a);
        logger.info("b = {}", b);
        logger.info("l = {}", l);
        logger.info("by = {}", by);

    }

    private void literalNumeric() {
        int a = 52_23;
        float pi = 3.14_15F;
        long hexBytes = 0xFF_EC_DE_5E;
        long hexWords = 0xCAFE_BABE;
        long maxLong = 0x7fff_ffff_ffff_ffffL;
        byte nybbles = 0b0010_0101;

        logger.info("Numeric Literals {}", a);
        logger.info("float pi {}:", pi);
        logger.info("hexBytes {}:", hexBytes);
        logger.info("hexWords {}:", hexWords);
        logger.info("maxLong {}:", maxLong);
        logger.info("nybbles {}:", nybbles);
    }

    private void stringSwitch() {
        Scanner scanner = new Scanner(System.in);
        logger.info("Enter the Country code:IND,USA,JPN,NZ,END.");

        String string = scanner.next();

        switch (string) {
            case "IND":
                logger.info("{} refers to INDIA", string);
                break;
            case "USA":
                logger.info("{} refers to UNITED STATES", string);
                break;
            case "JPN":
                logger.info("{} refers to JAPAN", string);
                break;
            case "SA":
                logger.info("{}  refers to SOUTH AFRICA", string);
                break;
            case "ENG":
                logger.info("{}  refers to ENGLAND", string);
                break;
            default:
                logger.info("{} Invalid code.", string);
                break;
        }
    }

    private static void genericInstance() {

        List<String> names = new ArrayList<>();
        names.add("Akshay");
        names.add("Roshan");
        names.add("Akhilesh");
        names.add("Sudarshan");
        names.add("Harshad");
        names.add("Ujwal");

        for (String name : names) logger.info("{}", name);
    }

    private void tryWithResource() {

        String query = "select * from users where id = 101";
        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {

                int id = resultSet.getInt("id");

                String name = resultSet.getString("name");

                int salary = resultSet.getInt("salary");

                logger.info("user id is {}, name {} and salary {}", id, name, salary);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }

    private void multipleCatch() {

        String query = "delete from users where id =?";
        char[] content = new char[30];
        try (FileReader fileReader = new FileReader("AdvanceOperations/src/main/resources/content.txt");
             Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);

             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setInt(1, 101);
            fileReader.read(content);

            logger.info("{}", String.valueOf(content));

            int result = statement.executeUpdate();
            logger.info("{} Record updated.", result);

        } catch (IOException | SQLException e) {
            logger.error(e.getMessage());
        }
    }

    private static void varargs(String... name) {

        for (String countryList : name) {

            logger.info("{}",countryList);
        }
    }
}
